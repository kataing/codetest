const Sequelize = require('sequelize');
const connection = require('./index');

// Defining the schema of the table
const Items = connection.define(
  'Items',
  {
    image: { type: Sequelize.STRING, allowedNull: false },
    flourType: { type: Sequelize.STRING, allowedNull: false },
    description: { type: Sequelize.STRING, allowedNull: false },
    factoid: { type: Sequelize.STRING, allowedNull: false },
  },
  { timestamps: false }
);

// Creates the model if it does not already exist
Items.sync()
  .then(() => {
    console.log('Items table has been synced.');
  })
  .catch((err) => {
    console.error(err);
  });

module.exports = Items;
