const Items = require('./model');

// Creating the data we want to seed the database with
const data = [
  {
    image: 'https://i.imgur.com/013tQDv.png',
    flourType: 'Unbleached All Purpose Flour',
    description:
      "On Day 1, you're going to mix equal parts water and flour (50g) each.",
    factoid:
      "Sourdough starters using Unbleached All Purpose Flour can have a more stringy texture (similar to melted cheese). If you notice water starting to collect at the top of the jar, don't fret, just feed your starter more often. ",
  },
  {
    image: 'https://i.imgur.com/jqqqDrw.png',
    flourType: 'Unbleached Bread Flour',
    description:
      "On Day 1, you're going to mix equal parts water and flour (50g) each.",
    factoid:
      'Sourdough starters using Unbleached Bread Flour can have a more stringy texture similar to starters using Unbleached All Purpose Flour. However, the husk left on ',
  },
  {
    image: 'https://i.imgur.com/DoybTgG.png',
    flourType: 'Whole Wheat Flour',
    description:
      "On Day 1, you're going to mix equal parts water (50g) and flour (25g All Purpose Flour & 25g Whole Wheat Flour).",
    factoid:
      'Sourdough starters using Unbleached Whole Wheat Flour can have a more stringy texture similar to starters using Unbleached All Purpose Flour. However, the husk left on Whole Wheat Flour promotes more yeast growth.',
  },
  {
    image: 'https://i.imgur.com/0sh9tDv.png',
    flourType: 'Rye Flour',
    description:
      "On Day 1, you're going to mix equal parts water (50g) and flour (25g All Purpose Flour & 25g Rye Flour).",
    factoid:
      'Sourdough starters using Rye Flour often mature faster. While often paste-like and dry, the husk on Rye Flour often yields the best results for increased yeast activity.',
  },
  {
    image: 'https://i.imgur.com/Oueom8l.png',
    flourType: 'Flour Blends',
    description:
      "On Day 1, you're going to mix equal parts water (50g) and flour (50g flour blend).",
    factoid:
      'Sourdough starters using a mix of different flours, can have a lower success rate, but the increased depth of flavor is well worth the challenge.',
  },
];

// Seeding the data into the database
Items.sync({ force: false })
  .then(() => {
    Items.bulkCreate(data)
      .then(() => {
        console.log('Data has been added.');
      })
      .catch(() => {
        console.error('Something went wrong. Data could not be added.');
      });
  })
  .catch(() => {
    console.err('Could not sync');
  });
