const Sequelize = require('sequelize');
const { host, database, user, password } = require('../config');

const connection = new Sequelize(
  database || process.env.DATABASE_NAME,
  user || process.env.DATABASE_USER,
  password || process.env.DATABASE_PASSWORD,
  {
    host: host || process.env.DATABASE_HOST,
    dialect: 'postgres',
    dialectOptions: {
      ssl: { require: true, rejectUnauthorized: false },
    },
  }
);

connection
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = connection;
