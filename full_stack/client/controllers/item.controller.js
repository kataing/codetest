class ItemController {
  constructor(itemService, itemView) {
    this.itemService = itemService;
    this.itemView = itemView;

    // Explicitly binding "this"
    this.itemService.bindItemListChanged(this.onItemListChanged);
    this.itemView.bindAddItem(this.handleAddItem);
    this.itemView.bindEditItem(this.handleEditItem);
    this.itemView.bindDeleteItem(this.handleDeleteItem);
    this.itemView.bindNextItem(this.handleNextItem);
    this.itemView.bindRestartItems(this.handleRestartItems);

    // Display initial items
    this.initializeItemlist();
  }

  initializeItemlist = async () => {
    await this.itemService.getItems();
    this.itemView.displayItem(
      this.itemService.currentItem,
      this.itemService.items
    );
  };

  onItemListChanged = () => {
    this.itemView.displayItem(
      this.itemService.currentItem,
      this.itemService.items
    );
  };

  handleAddItem = async (item) => {
    await this.itemService.addItem(item);
    this.initializeItemlist();
  };

  handleEditItem = (id, item) => {
    this.itemService.editItem(id, item);
  };

  handleDeleteItem = async (id) => {
    await this.itemService.deleteItem(id);
    this.onItemListChanged();
  };

  handleNextItem = () => {
    this.itemService.nextItem();
    this.onItemListChanged();
  };

  handleRestartItems = () => {
    this.itemService.restartItems();
    this.onItemListChanged();
  };
}
