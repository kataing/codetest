class ItemView {
  constructor() {
    this.app = this.getElement('#root');

    // Page Container: Contains all items on the page
    this.pageContainer = this.createElement('div', 'page-container');

    // Title
    this.title = this.createElement('h1', 'title');
    this.title.textContent = 'Sourdough Starters';

    // Paragraph: Page Blurb
    this.paragraph = this.createElement('p', 'paragraph');
    this.paragraph.textContent =
      "Sourdough starters take days to mature. You'll know it's fully mature when it doubles in size, and passes the float test. View the card for what to do on Day 1. On the Day 2 - 5, you're going to use a 1:1:1 ratio of starter, flour, and water. Every day after will be an experiment. If your starter bubbles quickly or has execess water on top, feed it more flour and water using anywhere from a 1:2:2 ratio to a 1:4:4 ratio. If your starter doesn't seem to change much from day to day increase the amount of starter you leave in the jar using a 2:1:1 ratio.";

    // Content Container: Allows card and form to display side-by-side
    this.contentContainer = this.createElement('div', 'content-container');

    // Card: The current card to display
    this.card = this.createElement('div', 'card');

    this.img = this.createElement('img', 'image');

    this.labelFlourType = this.createElement('label', 'form-subtitle');
    this.labelFlourType.textContent = 'Flour Type';

    this.spanFlourType = this.createElement('span');

    this.labelDescription = this.createElement('label', 'form-subtitle');
    this.labelDescription.textContent = 'Description';

    this.spanDescription = this.createElement('span');
    this.spanDescription.contentEditable = true;
    this.spanDescription.classList.add('editable');

    this.labelFactoid = this.createElement('label', 'form-subtitle');
    this.labelFactoid.textContent = 'Flour Type';

    this.spanFactoid = this.createElement('span');
    this.spanFactoid.contentEditable = true;
    this.spanFactoid.classList.add('editable');

    this.buttonContainer = this.createElement('div', 'button-container');

    this.deleteButton = this.createElement('button', 'button');
    this.deleteButton.textContent = 'Delete';

    this.nextButton = this.createElement('button', 'button');
    this.nextButton.textContent = 'Next';

    this.saveButton = this.createElement('button', 'button');
    this.saveButton.textContent = 'Save Changes';

    this.restartButton = this.createElement('button');
    this.restartButton.textContent = 'Start Again';

    // Form: Create New Card
    this.form = this.createElement('form', 'form');

    // Form Title
    this.formTitle = this.createElement('h2', 'form-title');
    this.formTitle.textContent = 'Create A New Sourdough Starter';

    // All Possible Flour Images
    this.images = [
      {
        image: 'https://i.imgur.com/013tQDv.png',
        flourType: 'Unbleached All Purpose Flour',
      },
      {
        image: 'https://i.imgur.com/jqqqDrw.png',
        flourType: 'Unbleached Bread Flour',
      },
      {
        image: 'https://i.imgur.com/DoybTgG.png',
        flourType: 'Whole Wheat Flour',
      },
      {
        image: 'https://i.imgur.com/0sh9tDv.png',
        flourType: 'Rye Flour',
      },
      {
        image: 'https://i.imgur.com/Oueom8l.png',
        flourType: 'Flour Blends',
      },
    ];

    this.formLabelFlourType = this.createElement('label', 'form-subtitle');
    this.formLabelFlourType.textContent = 'Flour Type';
    this.createSelect({
      key: 'selectFlourType',
      type: 'select',
      options: this.getOptions(),
    });

    this.formLabelDescription = this.createElement('label', 'form-subtitle');
    this.formLabelDescription.textContent = 'Description';
    this.createInput({
      key: 'inputDescription',
      type: 'text',
      placeholder: 'Description',
    });

    this.formLabelFactoid = this.createElement('label', 'form-subtitle');
    this.formLabelFactoid.textContent = 'Factoid';
    this.createInput({
      key: 'inputFactoid',
      type: 'text',
      placeholder: 'Factoid',
    });

    this.submitButton = this.createElement('button');
    this.submitButton.textContent = 'Submit';

    // Combine all the elements of the form
    this.form.append(
      this.formTitle,
      this.formLabelFlourType,
      this.selectFlourType,
      this.formLabelDescription,
      this.inputDescription,
      this.formLabelFactoid,
      this.inputFactoid,
      this.submitButton
    );

    this.contentContainer.append(this.card, this.form);

    // Combine all the elements of the page
    this.pageContainer.append(
      this.title,
      this.paragraph,
      this.contentContainer
    );
    this.app.append(this.pageContainer);

    // Form Variables
    // this._editFactoidText = '';
  }

  _resetInput() {
    this.selectFlourType.value = this.images[0].flourType;
    this.inputDescription.value = '';
    this.inputFactoid.value = '';
  }

  findImage(flourType) {
    for (let i = 0; i < this.images.length; i++) {
      if (this.images[i].flourType === flourType) {
        return this.images[i].image;
      }
    }
  }

  getOptions() {
    let options = [];
    for (let i = 0; i < this.images.length; i++) {
      options.push(this.images[i].flourType);
    }
    return options;
  }

  get _flourTypeText() {
    return this.selectFlourType.value;
  }

  get _descriptionText() {
    return this.inputDescription.value;
  }
  get _factoidText() {
    return this.inputFactoid.value;
  }

  get _editDescriptionText() {
    return this.spanDescription.textContent;
  }

  get _editFactoidText() {
    return this.spanFactoid.textContent;
  }

  bindAddItem(handler) {
    this.form.addEventListener('submit', (e) => {
      e.preventDefault();

      if (this._flourTypeText && this._descriptionText && this._factoidText) {
        handler({
          image: this.findImage(this._flourTypeText),
          flourType: this._flourTypeText,
          description: this._descriptionText,
          factoid: this._factoidText,
        });
        this._resetInput();
      }
    });
  }

  bindEditItem(handler) {
    this.saveButton.addEventListener('click', (e) => {
      let data = {};
      const id = e.target.id;
      if (this._editDescriptionText) {
        const key = 'description';
        data = { ...data, [key]: this._editDescriptionText };
      }
      if (this._editFactoidText) {
        const key = 'factoid';

        data = { ...data, [key]: this._editFactoidText };
      }
      handler(id, data);
    });
  }

  bindDeleteItem(handler) {
    this.deleteButton.addEventListener('click', (e) => {
      const id = e.target.id;

      handler(id);
    });
  }

  bindNextItem(handler) {
    this.nextButton.addEventListener('click', (e) => {
      handler();
    });
  }

  bindRestartItems(handler) {
    this.restartButton.addEventListener('click', (e) => {
      handler();
    });
  }

  // Gets element by using the element ID
  getElement(selector) {
    return document.querySelector(selector);
  }

  // Creates an HTML element
  createElement(tag, className) {
    const element = document.createElement(tag);
    if (className) element.classList.add(className);
    return element;
  }

  // Creates an HTML Input
  createInput(
    { key, type, placeholder } = {
      key: 'default',
      type: 'text',
      placeholder: 'default',
    }
  ) {
    this[key] = this.createElement('input');
    this[key].type = type;
    this[key].placeholder = placeholder;
  }

  // Creates HTML options for the Select element
  createOptions(options, selectList) {
    for (let i = 0; i < options.length; i++) {
      let option = document.createElement('option');
      option.value = options[i];
      option.text = options[i];
      selectList.appendChild(option);
    }
  }

  // Creates an HTML Select
  createSelect(
    { key, options } = {
      key: 'default',
      options: [],
    }
  ) {
    this[key] = this.createElement('select');
    this.createOptions(options, this[key]);
  }

  // Creates the list of cards
  displayItem(item, items) {
    // Delete all nodes
    while (this.card.firstChild) {
      this.card.removeChild(this.card.firstChild);
    }

    // Check if there are any items to display
    if (!items.length) {
      const paragraphNoCards = this.createElement('p');
      paragraphNoCards.textContent =
        'There are no items to display. Would you like to create one?';

      this.card.append(paragraphNoCards);
    } else if (!item) {
      const paragraphDeckEnd = this.createElement('p');
      paragraphDeckEnd.textContent =
        "You've gone through the deck, Would you like to start over?";

      this.card.append(paragraphDeckEnd, this.restartButton);
    } else {
      // Create each card
      const { id, image, flourType, description, factoid } = item;

      this.img.src = image;
      this.spanFlourType.textContent = flourType;
      this.spanDescription.textContent = description;
      this.spanFactoid.textContent = factoid;
      this.saveButton.id = id;

      // Card Buttons
      this.deleteButton.id = id;

      this.buttonContainer.append(
        this.saveButton,
        this.deleteButton,
        this.nextButton
      );

      this.card.append(
        this.img,
        this.labelFlourType,
        this.spanFlourType,
        this.labelDescription,
        this.spanDescription,
        this.labelFactoid,
        this.spanFactoid,
        this.buttonContainer
      );
    }
  }
}
