class ItemModel {
  constructor({ id, image, flourType, description, factoid }) {
    this.id = id;
    this.image = image;
    this.flourType = flourType;
    this.description = description;
    this.factoid = factoid;
  }
}
