class ItemService {
  constructor() {
    this.items = [];
    this.currentIndex = null;
    this.currentItem = null;
  }

  bindItemListChanged(callback) {
    this.onItemListChanged = callback;
  }

  // Grabs all existing items in the database
  getItems() {
    return fetch('/api/item')
      .then((res) => res.json())
      .then((items) => {
        this.items = items.map((item) => new ItemModel(item));
        this.currentIndex = 0;
        this.currentItem = items[this.currentIndex];
      })
      .catch(() => console.error('Failed to fetch items'));
  }

  // Adds a new item to the database
  addItem(item) {
    fetch('/api/item', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(item),
    })
      .then((response) => response.json())
      .then(() => {})
      .catch((err) => console.error('ERROR: Unable to add new item'));
  }

  // Edits an item
  editItem(id, itemToEdit) {
    this.items = this.items.map((item) =>
      item.id === id
        ? new ItemModel({
            ...item,
            ...itemToEdit,
          })
        : item
    );
    this.currentItem = this.items[this.currentIndex];

    fetch(`/api/id/${id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ...this.currentItem, ...itemToEdit }),
    })
      .then((response) => response.json())
      .then((data) => data)
      .catch((err) =>
        console.error('ERROR: Unable to make updates to the item')
      );
  }

  // Deletes an item
  deleteItem(itemId) {
    // Remove deleted item from memory
    this.items = this.items.filter(({ id }) => id !== Number(itemId));

    // Update the current item in memory
    if (this.items.length === 0) {
      this.currentIndex = null;
      this.currentItem = null;
    } else {
      this.currentItem = this.items[this.currentIndex];
    }

    // Remove deleted item from database
    fetch(`/api/id/${itemId}`, {
      method: 'DELETE',
    })
      .then((response) => response.json())
      .then((data) => data)
      .catch((err) => console.error('ERROR: Unable to delete item'));
  }

  nextItem() {
    // Handles if user is on the last card and clicks "Next"
    if (this.currentIndex === this.items.length - 1) {
      this.currentIndex = 0;
      this.currentItem = null;
    } else {
      this.currentIndex += 1;
      this.currentItem = this.items[this.currentIndex];
    }
  }

  restartItems() {
    // Starts deck over again
    this.currentIndex = 0;
    this.currentItem = this.items[this.currentIndex];
  }
}
