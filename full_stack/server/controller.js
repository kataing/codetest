const Items = require('../database/model');

const controller = {
  // Read all items
  get: (req, res) => {
    Items.findAll({})
      .then((data) => {
        res.status(200).send(data);
      })
      .catch((err) => {
        console.error(err);
        res
          .status(500)
          .send('Error: The server was unable to retrieve all the records.');
      });
  },
  // Delete a single item
  delete: (req, res) => {
    const { id } = req.params;
    // Checking to make sure id exists and is a number
    if (id !== null && !isNaN(id)) {
      Items.destroy({ where: { id } })
        .then(() => {
          res.status(200).send(`Item with id ${id} has been deleted.`);
        })
        .catch((err) => {
          console.error(err);
          res
            .status(500)
            .send('Error: The server was unable to delete this item.');
        });
    } else {
      res
        .status(400)
        .send(
          'Error: Item not deleted. Please make sure an id of type number is passed in as a param.'
        );
    }
  },
  // Create a single item
  post: (req, res) => {
    const { image, flourType, description, factoid } = req.body;
    if (
      typeof image === 'string' &&
      typeof flourType === 'string' &&
      typeof description === 'string' &&
      typeof factoid === 'string'
    ) {
      Items.create({ image, flourType, description, factoid })
        .then(() => {
          res.status(201).send('Your new item has been created.');
        })
        .catch((err) => {
          console.error(err);
          res
            .status(500)
            .send('Error: The server was unable to create your new item.');
        });
    } else {
      res
        .status(400)
        .send(
          'Error: Item not created. Please make sure all variables in req.body exist and are strings.'
        );
    }
  },
  // Update a single item
  update: (req, res) => {
    const { id } = req.params;
    const { image, flourType, description, factoid } = req.body;
    // Checking to make sure id exists and is a number
    if (id !== null && !isNaN(id)) {
      if (
        typeof image === 'string' &&
        typeof flourType === 'string' &&
        typeof description === 'string' &&
        typeof factoid === 'string'
      ) {
        Items.update(
          { image, flourType, description, factoid },
          { where: { id } }
        )
          .then(() => {
            res.status(201).send('Your update has been made.');
          })
          .catch((err) => {
            console.error(err);
            res
              .status(500)
              .send('Error: The server was unable to update this item.');
          });
      } else {
        res
          .status(400)
          .send(
            'Error: Item not updated. Please make sure all variables in req.body exist and are strings.'
          );
      }
    } else {
      res
        .status(400)
        .send(
          'Error: Item not updated. Please make sure an id of type number is passed in as a param.'
        );
    }
  },
};

module.exports = controller;
