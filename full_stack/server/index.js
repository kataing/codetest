const express = require('express');
const path = require('path');
const parser = require('body-parser');

// Database connection
const db = require('../database/index');

// Server routes
const router = require('./router');

const app = express();
const port = process.env.PORT || 3000;

app.use(parser.json());
app.use(parser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, '../client/')));

// Created an initial end point for the routes
app.use('/api', router);

app.listen(port, () => console.log(`You are now listening on port ${port}.`));

module.exports = app;
